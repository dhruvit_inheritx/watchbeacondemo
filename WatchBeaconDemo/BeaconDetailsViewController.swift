//
//  BeaconDetailsViewController.swift
//  WatchBeaconDemo
//
//  Created by Dhruvit on 05/04/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import UIKit
import CoreLocation

class BeaconDetailsViewController: UIViewController {

    var lastBeacon : CLBeacon!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        if lastBeacon != nil {
            print("proximityUUID :- %@",lastBeacon.proximityUUID)
            print("proximity :- %@",lastBeacon.proximity)
            print("accuracy :- %@",lastBeacon.accuracy)
            print("major :- %@",lastBeacon.major)
            print("minor :- %@",lastBeacon.minor)
            print("rssi :- %@",lastBeacon.rssi)
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
