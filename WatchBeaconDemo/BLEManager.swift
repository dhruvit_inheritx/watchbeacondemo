//
//  BLEManager.swift
//  WatchBeaconDemo
//
//  Created by Dhruvit on 04/04/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import UIKit
import CoreLocation
import SystemConfiguration

class BLEManager: NSObject, CLLocationManagerDelegate {
    
    var beaconRegion1 : CLBeaconRegion!
    var beaconRegion2 : CLBeaconRegion!
    var beaconRegion3 : CLBeaconRegion!
    
    var locationManager = CLLocationManager()
    
    let lastProximity = CLProximity(rawValue: 0)
    
    static let sharedInstance:BLEManager = {
        let instance = BLEManager()
        return instance
    } ()
    
    override init() {
        super.init()
        
        switch CLLocationManager.authorizationStatus() {
        case .authorizedAlways:
            print("Authorized Always")
            break;
        case .authorizedWhenInUse:
            print("Authorized when in use")
            break;
        case .denied:
            print("Denied")
            break;
        case .notDetermined:
            print("Not determined")
            break;
        case .restricted:
            print("Restricted")
            break;
        }
        
        locationManager = CLLocationManager()
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self
    }
    
    func startMonitoring()  {
        
        if let uuid = UUID(uuidString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D") {
            beaconRegion1 = CLBeaconRegion(
                proximityUUID: uuid,
                major: 49847,
                minor: 4828,
                identifier: "lemon")
            locationManager.startMonitoring(for: beaconRegion1)
            locationManager.startRangingBeacons(in: beaconRegion1)
        }
        
        if let uuid = UUID(uuidString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D") {
            beaconRegion2 = CLBeaconRegion(
                proximityUUID: uuid,
                major: 35812,
                minor: 12613,
                identifier: "beetroot")
            locationManager.startMonitoring(for: beaconRegion2)
            locationManager.startRangingBeacons(in: beaconRegion2)
        }
        
        if let uuid = UUID(uuidString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D") {
            beaconRegion3 = CLBeaconRegion(
                proximityUUID: uuid,
                major: 42571,
                minor: 23635,
                identifier: "candy")
            locationManager.startMonitoring(for: beaconRegion3)
            locationManager.startRangingBeacons(in: beaconRegion3)
        }
        
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.startUpdatingLocation()
        
//        let beaconUUID1 = UUID(uuidString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")
//        let strRegionId1 = "lemon"
//        beaconRegion1 = CLBeaconRegion(proximityUUID: beaconUUID1!, major: 49847, minor: 4828, identifier: strRegionId1)
//        
//        let beaconUUID2 = UUID(uuidString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")
//        let strRegionId2 = "beetroot"
//        beaconRegion2 = CLBeaconRegion(proximityUUID: beaconUUID2!, major: 35812, minor: 12613, identifier: strRegionId2)
//        
//        let beaconUUID3 = UUID(uuidString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")
//        let strRegionId3 = "candy"
//        beaconRegion3 = CLBeaconRegion(proximityUUID: beaconUUID3!, major: 42571, minor: 23635, identifier: strRegionId3)
//        
//        locationManager.startMonitoring(for: beaconRegion1)
//        locationManager.startRangingBeacons(in: beaconRegion1)
//        
//        locationManager.startMonitoring(for: beaconRegion2)
//        locationManager.startRangingBeacons(in: beaconRegion2)
//        
//        locationManager.startMonitoring(for: beaconRegion3)
//        locationManager.startRangingBeacons(in: beaconRegion3)
        
    }
    
    func stopMonitoring() {
        
        locationManager.stopUpdatingLocation()
        
        locationManager.stopMonitoring(for: beaconRegion1)
        locationManager.stopRangingBeacons(in: beaconRegion1)
        
        locationManager.stopMonitoring(for: beaconRegion2)
        locationManager.stopRangingBeacons(in: beaconRegion2)
        
        locationManager.stopMonitoring(for: beaconRegion3)
        locationManager.stopRangingBeacons(in: beaconRegion3)
    }
}
