//
//  ViewController.swift
//  WatchBeaconDemo
//
//  Created by Dhruvit on 04/04/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import UIKit
import Foundation
import CoreLocation
import WatchConnectivity
import AudioToolbox
import UserNotifications

class ViewController: UIViewController {
    
    var objBLEManager : BLEManager!
    let locationManager = CLLocationManager()
    var lastProximity = CLProximity(rawValue: 0)
    var arrayBeacons : [CLBeacon] = []
    
    var watchSession : WCSession!
    
    var viewFirst = UIView()
    var viewSecond = UIView()
    var viewThird = UIView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.loadSubViews()
        
//        objBLEManager = BLEManager.sharedInstance
//        objBLEManager.startMonitoring()
//        objBLEManager.locationManager.delegate = self
        
        //        self.perform(#selector(ViewController.vibrateDevice), with: nil, afterDelay: 10)
        
        self.sendMessageToWatchApp()        
    }
    
    @IBAction func sendMessageToWatchApp() {
        if WCSession.isSupported() {
            watchSession = WCSession.default()
            watchSession.delegate = self
            watchSession.activate()
            
            let dictData = ["username":"iPhone","pass":"123456"]
            
            watchSession.sendMessage(["data":dictData], replyHandler: { (response) in
                print("response :- ",response)
            }, errorHandler: { (error) in
                print("Error sending message : %@" ,error)
            })
        }
        else {
            print("WCSession not Supported.")
        }
    }
    
    func vibrateDevice() {
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    func loadSubViews() {
        
        viewFirst = UIView()
        viewFirst.frame = CGRect(x: 0, y: 0, width: 120, height: 120)
        viewFirst.alpha = 0.0
        viewFirst.clipsToBounds = true
        viewFirst.layer.cornerRadius = viewFirst.frame.size.width / 2
        viewFirst.backgroundColor = UIColor.clear
        viewFirst.layer.borderColor = UIColor.cyan.cgColor
        viewFirst.layer.borderWidth = 2.0
        viewFirst.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.view.addSubview(viewFirst)
        
        viewSecond = UIView()
        viewSecond.frame = CGRect(x: 0, y: 0, width: 160, height: 160)
        viewSecond.alpha = 0.0
        viewSecond.clipsToBounds = true
        viewSecond.layer.cornerRadius = viewSecond.frame.size.width / 2
        viewSecond.backgroundColor = UIColor.clear
        viewSecond.layer.borderColor = UIColor.cyan.cgColor
        viewSecond.layer.borderWidth = 1.5
        viewSecond.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.view.addSubview(viewSecond)
        
        viewThird = UIView()
        viewThird.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        viewThird.alpha = 0.0
        viewThird.clipsToBounds = true
        viewThird.layer.cornerRadius = viewThird.frame.size.width / 2
        viewThird.backgroundColor = UIColor.clear
        viewThird.layer.borderColor = UIColor.cyan.cgColor
        viewThird.layer.borderWidth = 3.0
        viewThird.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
        self.view.addSubview(viewThird)
        
        viewFirst.center = self.view.center
        viewSecond.center = self.view.center
        viewThird.center = self.view.center
        
        self.perform(#selector(self.startAnimation), with: nil, afterDelay: 1.0)
    }
    
    func startAnimation() {
        UIView.animateKeyframes(withDuration: 0.2, delay: 0.0, options: UIViewKeyframeAnimationOptions.calculationModeLinear, animations: {
            self.viewFirst.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            self.viewFirst.alpha = 1.0
        }) { (finished) in
            
            UIView.animateKeyframes(withDuration: 0.2, delay: 0.0, options: UIViewKeyframeAnimationOptions.calculationModeLinear, animations: {
                self.viewSecond.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                self.viewSecond.alpha = 1.0
            }) { (finished) in
                
                UIView.animateKeyframes(withDuration: 0.2, delay: 0.0, options: UIViewKeyframeAnimationOptions.calculationModeLinear, animations: {
                    self.viewThird.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
                    self.viewThird.alpha = 1.0
                }) { (finished) in
                    
                    self.perform(#selector(self.stopAnimation), with: nil, afterDelay: 0.6)
                }
            }
        }
    }
    
    func stopAnimation() {
        UIView.animateKeyframes(withDuration: 0.2, delay: 0.0, options: UIViewKeyframeAnimationOptions.calculationModeLinear, animations: {
            self.viewThird.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
            self.viewThird.alpha = 0.0
        }) { (finished) in
            
            UIView.animateKeyframes(withDuration: 0.2, delay: 0.0, options: UIViewKeyframeAnimationOptions.calculationModeLinear, animations: {
                self.viewSecond.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                self.viewSecond.alpha = 0.0
            }) { (finished) in
                
                UIView.animateKeyframes(withDuration: 0.2, delay: 0.0, options: UIViewKeyframeAnimationOptions.calculationModeLinear, animations: {
                    self.viewFirst.transform = CGAffineTransform(scaleX: 0.5, y: 0.5)
                    self.viewFirst.alpha = 0.0
                }) { (finished) in
                    
                    self.perform(#selector(self.startAnimation), with: nil, afterDelay: 0.6)
                }
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "pushToBeaconDetail" {
            let vc = segue.destination as! BeaconDetailsViewController
            
            if arrayBeacons.count > 0 {
                vc.lastBeacon = arrayBeacons.first
            }
        }
    }
    
    func openViewController(strIndentifier : String , strTitle : String) {
        
        DispatchQueue.main.async {
            self.performSegue(withIdentifier: strIndentifier, sender: self)
        }
    }
}

extension ViewController : WCSessionDelegate {
    
    @available(iOS 9.3, *)
    public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("activationDidCompleteWith")
    }
    
    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        print("didReceiveMessage")
        let dictData = message["data"] as! [String:Any]
        print("dictData :- \(dictData)")
        let strId = dictData["controller"] as! String
        let strTitle = dictData["title"] as! String
        self.openViewController(strIndentifier: strId, strTitle: strTitle)
        
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }
    
    public func sessionDidBecomeInactive(_ session: WCSession) {
        print("sessionDidBecomeInactive")
    }
    
    public func sessionDidDeactivate(_ session: WCSession) {
        print("sessionDidDeactivate")
    }
    
}

extension ViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        manager.startRangingBeacons(in: region as! CLBeaconRegion)
        objBLEManager.locationManager.startUpdatingLocation()
        print("You entered the region.")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        
        manager.stopRangingBeacons(in: region as! CLBeaconRegion)
        objBLEManager.locationManager.stopUpdatingLocation()
        print("You exited the region.")
        
        let notification = UILocalNotification()
        notification.alertBody = "Are you forgetting something?"
        notification.soundName = "Default"
        UIApplication.shared.presentLocalNotificationNow(notification)
    }
    
    func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        var strMessage = ""
        
        if beacons.count > 0 {
            
            arrayBeacons = beacons
            
            let nearestBeacon = beacons.first! as CLBeacon
            
            print("beacons :- \(beacons)")
            
            if nearestBeacon.proximity == lastProximity || nearestBeacon.proximity == CLProximity.unknown {
                return
            }
            
            self.lastProximity = nearestBeacon.proximity
            
            switch nearestBeacon.proximity {
            case .far:
                strMessage = "You are far away from the beacon"
                break;
            case .near:
                strMessage = "You are near the beacon"
                break;
            case .immediate:
                strMessage = "You are in the immediate proximity of the beacon"
                break;
            default:
                return
            }
            
//            let beaconRegion1 : CLBeaconRegion = BLEManager.sharedInstance.beaconRegion1
//            let beaconRegion2 : CLBeaconRegion = BLEManager.sharedInstance.beaconRegion2
//            let beaconRegion3 : CLBeaconRegion = BLEManager.sharedInstance.beaconRegion3
//            
//            if nearestBeacon.major ==  beaconRegion1.major || nearestBeacon.minor == beaconRegion1.minor {
//                print("lemon")
//                print(beaconRegion1.identifier)
//            }
//            else if nearestBeacon.major ==  beaconRegion2.major || nearestBeacon.minor == beaconRegion2.minor {
//                print("beetroot")
//                print(beaconRegion2.identifier)
//            }
//            else if nearestBeacon.major ==  beaconRegion3.major || nearestBeacon.minor == beaconRegion3.minor {
//                print("candy")
//                print(beaconRegion3.identifier)
//            }
            
            objBLEManager.locationManager.delegate = self
            
            self.performSegue(withIdentifier: "pushToBeaconDetail", sender: self)
            
        }
        else {
            strMessage = "No beacons are nearby"
        }
        
        print("Beacon msg :- \(strMessage)")
    }
}
