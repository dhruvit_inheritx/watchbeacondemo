//
//  InterfaceController.swift
//  WatchBeaconDemo WatchKit Extension
//
//  Created by Dhruvit on 04/04/17.
//  Copyright © 2017 Dhruvit. All rights reserved.
//

import WatchKit
import Foundation
import WatchConnectivity

class InterfaceController: WKInterfaceController {

    var watchSession : WCSession!
    var btn : WKInterfaceButton!
    
    override func awake(withContext context: Any?) {
        super.awake(withContext: context)
        
        // Configure interface objects here.
        
        if WCSession.isSupported() {
            watchSession = WCSession.default()
            watchSession.delegate = self
            watchSession.activate()
        }
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
    }
    
    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func btnOneClicked() {
        self.openController(strControllerNo: "segueToFirstVC", strTitle: "One")
    }
    
    @IBAction func btnTwoClicked() {
        self.openController(strControllerNo: "segueToSecondVC", strTitle: "Two")
    }
    
    @IBAction func btnThreeClicked() {
        self.openController(strControllerNo: "segueToThirdVC", strTitle: "Three")
    }
    
    @IBAction func btnFourClicked() {
        self.openController(strControllerNo: "segueToFourthVC", strTitle: "Four")
    }
    
    func openController(strControllerNo : String , strTitle : String) {
        if WCSession.isSupported() {
            watchSession = WCSession.default()
            watchSession.delegate = self
            watchSession.activate()
            
            let dictData = ["controller":strControllerNo,"title":strTitle]
            
            watchSession.sendMessage(["data":dictData], replyHandler: { (response) in
                print(response)
            }, errorHandler: { (error) in
                print("Error sending message : %@" ,error)
            })
        }
        else {
            print("WCSession not Supported.")
        }
    }

}

extension InterfaceController : WCSessionDelegate {
    
    @available(watchOSApplicationExtension 2.2, *)
    public func session(_ session: WCSession, activationDidCompleteWith activationState: WCSessionActivationState, error: Error?) {
        print("activationDidCompleteWith")
    }

    func session(_ session: WCSession, didReceiveMessage message: [String : Any], replyHandler: @escaping ([String : Any]) -> Void) {
        
        print("didReceiveMessage")
        let dictData = message["data"] as! [String:Any]
        print("dictData :- \(dictData)")
        WKInterfaceDevice.current().play(WKHapticType.notification)
    }
    
}
